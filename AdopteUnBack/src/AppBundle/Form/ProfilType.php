<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\File;

class ProfilType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('lastName', TextType::class, [
                    'label' => 'Nom de famille',
                ])
                ->add('firstName', TextType::class, [
                    'label' => 'Prénom',
                ])
                ->add('birthday', BirthdayType::class, [
                    'label' => 'Date de naissance',
                ])
                ->add('phone', TelType::class, [
                    'label' => 'Numéro de téléphone',
                ])
                ->add('fb', UrlType::class, [
                    'label' => 'Facebook',
                    'required' => false
                ])
                ->add('lkdn', UrlType::class, [
                    'label' => 'LinkedIn',
                    'required' => false
                ])
                ->add('twtr', UrlType::class, [
                    'label' => 'Twitter',
                    'required' => false
                ])
                ->add('insta', UrlType::class, [
                    'label' => 'Instagram',
                    'required' => false
                ])
                ->add('github', UrlType::class, [
                    'label' => 'GitHub',
                    'required' => false
                ])
                ->add('gitlab', UrlType::class, [
                    'label' => 'GitLab',
                    'required' => false
                ])
                ->add('so', UrlType::class, [
                    'label' => 'Stack Overflow',
                    'required' => false
                ])
                ->add('other', HiddenType::class, [
                    'label' => 'Autres',
                    'required' => false
                ])
                ->add('pdfCv', FileType::class, [
                    'label' => 'CV Pdf',
                    'required' => false,
                    'data_class' => null,
                    'attr' => [
                        'accept' => '.pdf',
                    ],
    
                    // unmapped fields can't define their validation using annotations
                    // in the associated entity, so you can use the PHP constraint classes
                    'constraints' => [
                        new File([
                            'mimeTypes' => [
                                'application/pdf',
                                'application/x-pdf',
                            ],
                            'mimeTypesMessage' => 'Merci d\'inserer un fichier PDF',
                        ])
                    ],
                ])
                ->add('linkCv', UrlType::class, [
                    'label' => 'Lien du cv',
                    'required' => false
                ])
                ->add('video', UrlType::class, [
                    'label' => 'Vidéo',
                    'required' => false
                ])
                ->add('promo', TextType::class, [
                    'label' => 'Promotion actuelle',
                ])
                ->add('intro', TextareaType::class, [
                    'label' => 'Introduction personnelle',
                    'required' => false
                ])
                ->add('city', TextType::class, [
                    'label' => 'Ville',
                ])
                ->add('img', FileType::class, [
                    'label' => 'Photo de profil',
                    'required' => false,
                    'data_class' => null
                ])
                ->add('save', SubmitType::class, [
                    'label' => 'Enregistrer',
                ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Profil'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_profil';
    }


}
