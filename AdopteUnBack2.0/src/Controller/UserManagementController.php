<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\AdminUserType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("admin/users")
 */
class UserManagementController extends AbstractController
{
    private $usersPerPage = 25;

    private function isPageAvailable($index) {
        if ($index < 1) return false;

        $repo = $this->getDoctrine()->getRepository(User::class);
        $nbUsers = $repo->getCount();

        return $nbUsers > $this->usersPerPage * ($index - 1);
    }

    /**
     * @Route("/", name="users.list")
     */
    public function listUsers()
    {
        $repo = $this->getDoctrine()->getRepository(User::class);
        $users = $repo->findBy([], [], $this->usersPerPage);

        return $this->render('users/gestion_user_list.html.twig', [
            'users' => $users
        ]);
    }

    /**
     * @Route("/page/{index}", name="users.list.pagination")
     */
    public function listUsersPerPage($index)
    {
        $index = intval($index) - 1;
        $offset = $this->usersPerPage * $index;

        if ($this->isPageAvailable($index)) {
            $repo = $this->getDoctrine()->getRepository(User::class);
            $users = $repo->findBy([], [], $this->usersPerPage, $offset);
    
            return $this->render('users/gestion_user_list.html.twig', [
                'users' => $users
            ]);
        }

        return $this->redirectToRoute('users.list');
    }

    /**
     * @Route("/add", name="users.create")
     */
    public function createUser(Request $request)
    {
        $manager = $this->getDoctrine()->getManager();
        $user = $this->get('fos_user.user_manager')->createUser();

        $form = $this->createForm(AdminUserType::class, $user, [
            'action' => $this->generateUrl(
                'users.create')
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setCreatedAt(new DateTime());
            $user->setEnabled(true);

            $manager->persist($user);
            $manager->flush();
            if($request->isXmlHttpRequest()){
                $html = $this->render('users/gestion_user_list_line.html.twig', ['user'=>$user, 'vue'=>''])->getContent();
                return new JsonResponse([
                    'status' => 200,
                    'response' => [
                        'type' => 'added',
                        'html' => $html,
                    ]
                ]);
            }
            return $this->redirectToRoute('users.list');
        }

        if($request->isXmlHttpRequest()) {
            return $this->render('users/gestion_user_form.html.twig', array(
                'form_create' => $form->createView()
            ));
        }
        return $this->render('users/gestion_user_create_edit.html.twig', array(
            'form_create' => $form->createView(),
            'isEdit'      => false
        ));
    }

    /**
     * @Route("/edit/{id}", name="users.edit")
     */
    public function editUser(User $user, Request $request)
    {
        $form = $this->createForm(AdminUserType::class, $user, [
        'action' => $this->generateUrl(
            'users.edit', ['id' => $user->getId() ])
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            if($request->isXmlHttpRequest()){
                $profil = $user->getProfil() !== null ? $user->getProfil()->getFirstName().' '.$user->getProfil()->getLastName() : null;
                return new JsonResponse([
                    'status' => 200,
                    'response' => [
                        'type' => 'updated',
                        'id' => $user->getId(),
                        'username' => $user->getUsername(),
                        'email' => $user->getEmail(),
                        'profil' => $profil,
                    ]
                ]);
            }

            return $this->redirectToRoute('users.list');
        }

        if($request->isXmlHttpRequest()){
            return $this->render('users/gestion_user_form.html.twig', array(
                'form_create' => $form->createView()
            ));
        }

        return $this->render('users/gestion_user_create_edit.html.twig', array(
            'form_create' => $form->createView(),
            'isEdit'      => true
        ));
    }

    /**
     * @Route("/delete/{id}", name="users.delete")
     */
    public function deleteUser(User $user, Request $request) {
        if (isset($user)) {
            $id = $user->getId();
            $manager = $this->getDoctrine()->getManager();
            $manager->remove($user);
            $manager->flush();
            if($request->isXmlHttpRequest()){
                return new JsonResponse([
                    'status' => 200,
                    'response' => [
                        'id' => $id
                    ]
                ]);
            }

            return $this->redirectToRoute('users.list');
        }

        return new Response("User  not found", 500);
    }

    /**
     * @Route("/enable/{id}", name="users.enable")
     */
    public function enableUser(User $user) {
        if (isset($user)) {
            $em = $this->getDoctrine()->getManager();
            $user->setEnabled(true);
            $em->flush();

            return $this->redirectToRoute('users.list');
        }

        return new Response("User  not found", 500);
    }
}
